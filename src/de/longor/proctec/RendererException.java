package de.longor.proctec;

@SuppressWarnings("serial")
public class RendererException extends Exception
{
	public RendererException(String string) {
		super(string);
	}
	
	public RendererException(String string, Throwable cause) {
		super(string, cause);
	}
}
