package de.longor.proctec;

import java.nio.ByteBuffer;

import com.google.gson.JsonObject;

import de.longor.util.JsonUtils;

public class RawImage
{
	/**
	 * The width (in pixels) of this RawImage.
	 **/
	public final int width;
	
	/**
	 * The height (in pixels) of this RawImage.
	 **/
	public final int height;
	
	/**
	 * The surface-area (in pixels) of this RawImage.
	 **/
	public final int surface;
	
	public final float[] imageData;
	
	/**
	 * Creates a new RawImage.
	 **/
	public RawImage(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.surface = width * height;
		this.imageData = new float[surface * 4];
	}
	
	public static RawImage createFromJson(JsonObject info$raw)
	{
		int width = JsonUtils.getInteger(info$raw, "width", -1);
		int height = JsonUtils.getInteger(info$raw, "height", -1);
		
		if(width <= 0 || height <= 0)
		{
			throw new RuntimeException("Unable to create RawImage instance: Widht or height is smaller or equal than zero.");
		}
		
		return new RawImage(width, height);
	}
	
	public static ByteBuffer convertTo_INT_RGBA(RawImage image)
	{
		ByteBuffer buffer = ByteBuffer.allocateDirect(image.surface*Integer.BYTES);
		for(float fchannel : image.imageData)
		{
			float multiplied = fchannel * 256f;
			multiplied = multiplied < 255f ? (multiplied > 0 ? multiplied : 0) : 255f;
			byte b = (byte) (((int) multiplied) & 0xFF);
			buffer.put(b);
		}
		buffer.flip();
		return buffer;
	}
	
}
