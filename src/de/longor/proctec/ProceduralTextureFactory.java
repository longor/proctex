package de.longor.proctec;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de.longor.util.JsonUtils;

public class ProceduralTextureFactory
{
	public static RawImage generateTexture(File file) throws IOException, RendererException
	{
		Reader in = new FileReader(file);
		RawImage img = generateTexture(in);
		in.close();
		return img;
	}
	
	public static RawImage generateTexture(URL url) throws IOException, RendererException
	{
		InputStream in = url.openStream();
		RawImage img = generateTexture(in);
		in.close();
		return img;
	}
	
	public static RawImage generateTexture(InputStream input) throws IOException, RendererException
	{
		return generateTexture(new InputStreamReader(input));
	}
	
	public static RawImage generateTexture(Reader input) throws IOException, RendererException
	{
		// read data
		JsonElement raw$root = new JsonParser().parse(input);
		
		// make sure root is a JsonObject
		if(!raw$root.isJsonObject())
			throw new IOException("Unable to generate texture: Given Json data is not packed as Json-Object.");
		
		// fetch data
		JsonObject root = raw$root.getAsJsonObject();
		JsonObject info = JsonUtils.getObject(root, "info", null);
		JsonArray layers = JsonUtils.getJsonArray(root, "layers", null);
		
		// check data
		if(info == null)
			throw new IOException("Unable to generate texture: Missing 'info' in root.");
		if(layers == null)
			throw new IOException("Unable to generate texture: Missing 'layers' in root.");
		
		// create empty image
		RawImage image = RawImage.createFromJson(info);
		
		ImageRenderer renderer = new ImageRenderer(image);
		renderer.renderLayers(layers);
		ImageRenderer.mergeLayers(renderer.currentLayer, image, null);
		
		return image;
	}
	
}
