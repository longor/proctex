package de.longor.proctec;

import java.util.Random;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import de.longor.util.JsonUtils;

public class ImageRenderer
{
	final int width;
	final int height;
	final float pixelwidth;
	final float pixelheight;
	RawImage outputData;
	RawImage currentLayer;
	
	public ImageRenderer(RawImage image)
	{
		width = image.width;
		height = image.height;
		pixelwidth = 1F / (float)width;
		pixelheight = 1F / (float)width;
		outputData = image;
	}
	
	public RawImage renderLayers(JsonArray layers) throws RendererException
	{
		System.out.println("Rendering layers: " + layers);
		currentLayer = new RawImage(width, height);
		
		for(JsonElement elm : layers)
		{
			if(elm.isJsonObject())
			{
				JsonObject layer = elm.getAsJsonObject();
				String type = JsonUtils.getString(layer, "type", null);
				
				if(type == null)
				{
					throw new RendererException("Layer is missing type.");
				}
				
				if(type.equals("noise"))
				{
					renderNoise(layer);
					continue;
				}
				
				if(type.equals("grid"))
				{
					renderGrid(layer);
					continue;
				}
				
				throw new RendererException("Layer has unknown type: " + type);
			}
		}
		
		return currentLayer;
	}
	
	private void renderGrid(JsonObject layer)
	{
		float[] color = JsonUtils.getFloatArray(layer, "color", new float[]{1,1,1,1}, 4);
		int linewidth = JsonUtils.getInteger(layer, "linewidth", 1);
		int cellx = JsonUtils.getInteger(layer, "cellx", 16);
		int celly = JsonUtils.getInteger(layer, "celly", 16);
		
		if(linewidth == 1)
		{
			for(int y = 0; y < currentLayer.height; y += celly)
			{
				for(int x = 0, ix = 0; x < currentLayer.width; x++)
				{
					ix = (x + y * currentLayer.width) * 4;
					currentLayer.imageData[ix+0] = color[0];
					currentLayer.imageData[ix+1] = color[1];
					currentLayer.imageData[ix+2] = color[2];
					currentLayer.imageData[ix+3] = color[3];
				}
			}
			
			for(int x = 0; x < currentLayer.width; x += celly)
			{
				for(int y = 0, ix = 0; y < currentLayer.height; y++)
				{
					ix = (x + y * currentLayer.width) * 4;
					currentLayer.imageData[ix+0] = color[0];
					currentLayer.imageData[ix+1] = color[1];
					currentLayer.imageData[ix+2] = color[2];
					currentLayer.imageData[ix+3] = color[3];
				}
			}
			return;
		}
		
	}
	
	private void renderNoise(JsonObject layer)
	{
		System.out.println("Rendering noise: " + layer);
		
		float rangeMin = JsonUtils.getFloat(layer, "range_min", 0f);
		float rangeMax = JsonUtils.getFloat(layer, "range_max", 1f);
		float saturation = JsonUtils.getFloat(layer, "saturation", 1f);
		float range = rangeMax - rangeMin;
		Random random = new Random();
		
		System.out.println("rangeMin is " + rangeMin + ".");
		System.out.println("rangeMax is " + rangeMax + ".");
		System.out.println("range is " + range + ".");
		System.out.println("Saturation is " + saturation + ".");
		
		if(saturation <= 0f)
		{
			float rgb = 1f,a = 1f;
			for(int i = 0, ix = 0; i < currentLayer.surface; i++, ix = i * 4)
			{
				rgb = rangeMin + random.nextFloat() * range;
				currentLayer.imageData[ix+0] = rgb;
				currentLayer.imageData[ix+1] = rgb;
				currentLayer.imageData[ix+2] = rgb;
				currentLayer.imageData[ix+3] = a;
			}
		}
		else if(saturation >= 1f)
		{
			float r = 1f,g = 1f, b = 1f,a = 1f;
			for(int i = 0, ix = 0; i < currentLayer.surface; i++, ix = i * 4)
			{
				r = rangeMin + random.nextFloat() * range;
				g = rangeMin + random.nextFloat() * range;
				b = rangeMin + random.nextFloat() * range;
				currentLayer.imageData[ix+0] = r;
				currentLayer.imageData[ix+1] = g;
				currentLayer.imageData[ix+2] = b;
				currentLayer.imageData[ix+3] = a;
			}
		}
		else
		{
			float rgb = 1f,r = 1f,g = 1f, b = 1f,a = 1f;
			for(int i = 0, ix = 0; i < currentLayer.surface; i++, ix = i * 4)
			{
				rgb = rangeMin + random.nextFloat() * range;
				
				r = rangeMin + random.nextFloat() * range;
				g = rangeMin + random.nextFloat() * range;
				b = rangeMin + random.nextFloat() * range;
				
				currentLayer.imageData[ix+0] = lerp(rgb, r, saturation);
				currentLayer.imageData[ix+1] = lerp(rgb, g, saturation);
				currentLayer.imageData[ix+2] = lerp(rgb, b, saturation);
				currentLayer.imageData[ix+3] = a;
			}
		}
	}
	
	public static final float lerp(float a, float b, float x)
	{
		return a + (x * (b - a));
	}
	
	public static final void mergeLayers(RawImage src, RawImage dst, JsonObject mergerules)
	{
		// We can't merge layers yet, so just copy SRC into DST.
		System.arraycopy(src.imageData, 0, dst.imageData, 0, src.imageData.length);
	}
	
}
