package de.longor.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class JsonUtils
{
	
	public static final boolean getBoolean(JsonObject container, String memberName, boolean defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isBoolean())
		{
			return prim.getAsBoolean();
		}
		
		return defaultValue;
	}
	
	public static final byte getBoolean(JsonObject container, String memberName, byte defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isNumber())
		{
			return prim.getAsByte();
		}
		
		return defaultValue;
	}
	
	public static final int getInteger(JsonObject container, String memberName, int defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isNumber())
		{
			return prim.getAsInt();
		}
		
		return defaultValue;
	}
	
	public static final float getFloat(JsonObject container, String memberName, float defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isNumber())
		{
			return prim.getAsFloat();
		}
		
		return defaultValue;
	}
	
	public static final double getDouble(JsonObject container, String memberName, double defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isNumber())
		{
			return prim.getAsDouble();
		}
		
		return defaultValue;
	}
	
	public static final long getLong(JsonObject container, String memberName, long defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isNumber())
		{
			return prim.getAsLong();
		}
		
		return defaultValue;
	}
	
	public static final String getString(JsonObject container, String memberName, String defaultValue)
	{
		JsonPrimitive prim = container.getAsJsonPrimitive(memberName);
		
		if(prim == null)
		{
			return defaultValue;
		}
		
		if(prim.isString())
		{
			return prim.getAsString();
		}
		
		return defaultValue;
	}

	public static JsonArray getJsonArray(JsonObject container, String memberName, JsonArray defaultValue)
	{
		JsonArray ary = container.getAsJsonArray(memberName);
		
		if(ary == null)
		{
			if(defaultValue == null)
			{
				return new JsonArray();
			}
			else
			{
				return defaultValue;
			}
		}
		
		return ary;
	}

	public static boolean hasFlag(JsonObject container, String memberName)
	{
		JsonElement element = container.get(memberName);
		
		if(element == null)
		{
			return false;
		}
		
		if(element.isJsonNull())
		{
			return true;
		}
		
		return false;
	}

	public static JsonObject getObject(JsonObject container, String memberName, JsonObject defaultValue) {
		JsonElement element = container.get(memberName);
		
		if(element == null)
		{
			return defaultValue;
		}
		
		if(element.isJsonObject())
		{
			return element.getAsJsonObject();
		}
		
		return defaultValue;
	}
	
	public static String[] getStringArray(JsonObject container, String memberName, String[] defaultValue)
	{
		JsonElement element = container.get(memberName);
		
		if(element == null)
		{
			return defaultValue;
		}
		
		if(!element.isJsonArray())
		{
			return defaultValue;
		}
		
		JsonArray array = element.getAsJsonArray();
		
		if(array.size() == 0)
			return defaultValue;
		
		String[] out = new String[array.size()];
		
		for(int i = 0; i < out.length; i++)
		{
			JsonElement elm = array.get(i);
			
			if(elm.isJsonPrimitive())
			{
				JsonPrimitive prim = elm.getAsJsonPrimitive();
				
				if(prim.isString())
				{
					out[i] = prim.getAsString();
					continue;
				}
			}
			
			return defaultValue;
		}
		
		return out;
	}
	
	public static float[] getFloatArray(JsonObject container, String memberName, float[] defaultValue, int requiredSize)
	{
		JsonElement element = container.get(memberName);
		
		if(element == null)
		{
			return defaultValue;
		}
		
		if(!element.isJsonArray())
		{
			return defaultValue;
		}
		
		JsonArray array = element.getAsJsonArray();
		
		if(array.size() == 0)
			return defaultValue;
		
		float[] out = new float[array.size()];
		
		if(requiredSize != -1 && array.size() < requiredSize)
		{
			return defaultValue;
		}
		
		for(int i = 0; i < out.length; i++)
		{
			JsonElement elm = array.get(i);
			
			if(elm.isJsonPrimitive())
			{
				JsonPrimitive prim = elm.getAsJsonPrimitive();
				
				if(prim.isNumber())
				{
					out[i] = prim.getAsFloat();
					continue;
				}
			}
			
			return defaultValue;
		}
		
		return out;
	}
	
}
